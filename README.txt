Help Text Editor

== How to use this module ==

Once this module is enabled and a user has the "edit entity help text" permission, that user can go to "/helptext_edit" and edit the help text of the configured fields.

== Todo ==

- Integration with the "node_title_help_text" module
- Handling for multilingual (help text is just run through the localization system, so editors of multilingual help text currently would need access to translate interface strings)

== Sponsors ==

Coldfront Labs Inc.
https://coldfrontlabs.ca
