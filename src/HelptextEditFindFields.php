<?php

namespace Drupal\helptext_edit;

use Drupal\field\Entity\FieldConfig;
use Drupal\Core\Entity\ContentEntityType;

trait HelptextEditFindFields {
  function getEntitiesWithHelpText() {
    $entities = [];
    foreach ($this->entityTypeManager->getDefinitions() as $entity_type => $definition) {
      if ($definition instanceof ContentEntityType
        and $this->getBundlesWithHelpText($entity_type))
      {
        $entities[$entity_type] = $definition->getBundleLabel();
      }
    }
    return $entities;
  }

  function getBundlesWithHelpText($entity_type) {
    $bundleInfo = $this->entityTypeBundleInfo->getBundleInfo($entity_type);
    if (!$bundleInfo) {
      return FALSE;
    }
    $bundles = [];
    foreach ($bundleInfo as $bundle => $label) {
      $fields = $this->getFieldWithHelpText($entity_type, $bundle);
      if (!empty($fields)) {
        $bundles[$bundle] = $label;
      }
    }
    return $bundles;
  }

  function getFieldWithHelpText($entity_type, $bundle) {
    $helptext = [];
    $fields = $this->entityFieldManager->getFieldDefinitions($entity_type, $bundle);
    if (!$fields) {
      return [];
    }
    foreach ($fields as $field_name => $definition) {
      $field_config = FieldConfig::loadByName($entity_type, $bundle, $field_name);
      if ($field_config) {
        $helptext[$field_name] = [
          'label' => $definition->getLabel(),
          'description' => $field_config->getDescription(),
        ];
      }
    }
    return $helptext;
  }

  function setHelpText($text, $entity_type, $bundle, $field_name) {
    $field_config = FieldConfig::loadByName($entity_type, $bundle, $field_name);
    if (!$field_config) {
      return FALSE;
    }
    $field_config->setDescription($text);
    drupal_set_message(print_r($field_config, TRUE));
    return $field_config->save();
  }

  function setEntityTypeManager($entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
    return $this;
  }

  function setEntityTypeBundleInfo($entityTypeBundleInfo) {
    $this->entityTypeBundleInfo = $entityTypeBundleInfo;
    return $this;
  }

  function setEntityFieldManager($entityFieldManager) {
    $this->entityFieldManager = $entityFieldManager;
    return $this;
  }
}
