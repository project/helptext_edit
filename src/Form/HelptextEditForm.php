<?php
/**
 * @file
 * Contains \Drupal\helptext_edit\Form\HelptextEditForm.
 */
namespace Drupal\helptext_edit\Form;

use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\helptext_edit\HelptextEditFindFields;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Implements an example form.
 */
class HelptextEditForm extends FormBase {
  use HelptextEditFindFields;
  /**
   * @var EntityFieldManager $entityFieldManager
   */
  protected $entityFieldManager;

  /**
   * Class constructor.
   */
  public function __construct(EntityFieldManager $entityFieldManager) {
    $this->setEntityFieldManager($entityFieldManager);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'helptext_edit_edit_form';
  }

  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $entity_type = NULL, $bundle = NULL) {
    $form['#entity_type_id'] = $entity_type;
    $form['#bundle_id'] = $bundle;

    $fields = $this->getFieldWithHelpText($entity_type, $bundle);
    if (empty($fields)) {
      throw new NotFoundHttpException();
    }

    foreach ($fields as $field_name => $definition) {
      $form[$field_name] = [
        '#type' => 'textarea',
        '#default_value' => $definition['description'],
        '#title' => $this->t('Help text for @field', ['@field' => $definition['label']]),
      ];
    }

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $fields = $this->getFieldWithHelpText($form['#entity_type_id'], $form['#bundle_id']);

    foreach ($fields as $field_name => $definition) {
      $this->setHelpText(
        $form_state->getValue($field_name),
        $form['#entity_type_id'],
        $form['#bundle_id'],
        $field_name
      );
    }
    \Drupal::messenger()->addStatus($this->t('Help text for @bundle (@entity) saved.', ['@entity' => $form['#entity_type_id'], '@bundle' => $form['#bundle_id']]));
  }
}
