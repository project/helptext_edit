<?php
namespace Drupal\helptext_edit\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeBundleInfo;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Link;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Drupal\helptext_edit\HelptextEditFindFields;

/**
 * An example controller.
 */
class HelptextEditController extends ControllerBase {
  use HelptextEditFindFields;

  protected $entityTypeManager;
  protected $entityTypeBundleInfo;

  public function __construct(
    EntityTypeManager $entityTypeManager,
    EntityTypeBundleInfo $entityTypeBundleInfo,
    EntityFieldManager $entityFieldManager
  ) {
    $this->setEntityTypeManager($entityTypeManager);
    $this->setEntityTypeBundleInfo($entityTypeBundleInfo);
    $this->setEntityFieldManager($entityFieldManager);
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * Returns a render-able array for a test page.
   */
  public function entityList() {
    foreach ($this->getEntitiesWithHelpText() as $entity_type => $label) {
      $link = Link::createFromRoute($label, 'helptext_edit.entity.bundle', ['entity_type' => $entity_type]);
      $items[] = $link->toRenderable();
    }

    $build['list'] = [
      '#theme' => 'item_list',
      '#items' => $items,
    ];
    return $build;
  }

  public function bundleList($entity_type, Request $request) {
    $items = [];
    $bundleInfo = $this->getBundlesWithHelpText($entity_type);
    if (!$bundleInfo) {
      throw new NotFoundHttpException();
    }
    foreach ($bundleInfo as $bundle => $label) {
      $link = Link::createFromRoute($label['label'], 'helptext_edit.entity.edit', ['entity_type' => $entity_type, 'bundle' => $bundle]);
      $items[] = $link->toRenderable();
    }

    $build['list'] = [
      '#theme' => 'item_list',
      '#items' => $items,
    ];
    return $build;
  }
}
